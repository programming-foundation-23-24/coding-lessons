﻿using UnityEngine;
using System;

[Serializable]
public class SpawnableData
{
    public GameObject prefab;
    public string text;
    public bool specialObject;
}
