using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;



public class SpawnerByQuantity : MonoBehaviour
{
    public List<UnitData> allObjectsData;

    [Header("Runtime only")]
    public int total;
    public int randomIndex;

    void Awake()
    {
        foreach (UnitData objectData in allObjectsData)
        {           
            total += objectData.unitProbability;
        }
    }

    public void SpawnByProbability()
    {
        randomIndex = Random.Range(0, total);
        int currentIndex = 0;

        /*
        for (int index = 0; index < allObjectsData.Count; index++)
        {
            ObjectData objectData = allObjectsData[index];

            currentIndex += objectData.quantity;

            if (randomIndex < currentIndex)
            {
                PickData(objectData);
                break;
            }
        }
        */

        foreach (UnitData objectData in allObjectsData)
        {
            currentIndex += objectData.unitProbability;

            if (randomIndex < currentIndex)
            {
                PickData(objectData);
                break;
            }
        }        
    }

    void PickData(UnitData pickedData)
    {
        Debug.Log($"Rolled {randomIndex}. {pickedData.unitName} has been spawned!");
        Instantiate(pickedData.unitPrefab);
    }
}
