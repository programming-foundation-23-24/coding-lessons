﻿using System;
using UnityEngine;

[Serializable]
public class UnitData
{
    public BaseUnit unitPrefab;
    public string unitName = "INSERT_NAME_HERE";
    public int unitProbability = 0;
}
