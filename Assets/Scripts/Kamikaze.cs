﻿using UnityEngine;

public class Kamikaze : BaseUnit
{
    public GameObject vfx;

    public override void Attack()
    {
        //Instantiate(vfx, transform.position, Quaternion.identity);
        Debug.Log($"{unitName} BOOM");
        Destroy(gameObject);
    }
}
