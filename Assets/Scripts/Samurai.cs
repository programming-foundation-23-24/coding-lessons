﻿using UnityEngine;

public class Samurai : BaseUnit
{
    public Animator animator;

    public override void Attack()
    {
        animator.SetTrigger("Attack");
    }
}
