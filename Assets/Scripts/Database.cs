﻿using System.Collections.Generic;
using UnityEngine;


public class Database : MonoBehaviour
{
    public Transform specialObjects;
    public List<SpawnableData> database;
    public bool buttonHasBeenClicked = false;

    [Header("Debug only")]
    public int counter = -1;

    //Editor time only (no build, no runtime)
    void OnDrawGizmos()
    {
        if (buttonHasBeenClicked)
        {
            counter++;

            if (IsSpecialObject())
            {
                Instantiate(GetPrefabToSpawn(), specialObjects);
            }
            else
            {
                Instantiate(GetPrefabToSpawn());
            }
            
            Debug.Log(GetCurrentTextByCounter());
            buttonHasBeenClicked = false;
        }
    }

    GameObject GetPrefabToSpawn()
    {
        if (CounterIsInRange())
        {
            return database[counter].prefab;
        }
        else
        {
            return database[database.Count - 1].prefab;
        }
    }


    string GetCurrentTextByCounter()
    {
        if (CounterIsInRange())
        {
            return database[counter].text;
        }
        else
        {
            return database[database.Count - 1].text;
        }
    }

    bool IsSpecialObject()
    {
        if (CounterIsInRange())
        {
            return database[counter].specialObject;
        }
        else
        {
            return database[database.Count - 1].specialObject;
        }
    }

    bool CounterIsInRange()
    {
        return counter <= database.Count - 1;
    }


}
