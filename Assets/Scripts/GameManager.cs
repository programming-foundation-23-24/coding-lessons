using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class GameManager : Singleton<GameManager>
{
    public event Action OnUnitSpawnRequested;
    public event Action OnAllUnitsAttackRequested;
    public event Action<string> OnUnitSpawnByNameRequested;

    protected override void Awake()
    {
        base.Awake();

        //qui eseguo il codice specifico di GameManager in awake...
    }

    public void RequestUnitSpawn()
    {
        OnUnitSpawnRequested?.Invoke();
    }

    public void RequestAllUnitsAttack()
    {
        OnAllUnitsAttackRequested?.Invoke();
    }

    public void RequestUnitySpawnByName(string unitName)
    {
        OnUnitSpawnByNameRequested?.Invoke(unitName);
    }
}
