﻿using UnityEngine;

public class Archer : BaseUnit
{
    public Projectile projectile;

    public override void Attack()
    {
        Instantiate(projectile, transform.position, Quaternion.identity);
    }
}


