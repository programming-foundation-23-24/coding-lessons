﻿using UnityEngine;

public class Pacifist : BaseUnit
{
    public override void Attack()
    {
        Debug.Log($"{unitName} dice: non picchiarmi!");
    }
}
