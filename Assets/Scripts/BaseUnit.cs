﻿using UnityEngine;




public abstract class BaseUnit : MonoBehaviour
{
    public string unitName;

    protected virtual void Start()
    {
        GameManager.Instance.OnAllUnitsAttackRequested += Attack;
    }

    protected virtual void OnDestroy()
    {
        GameManager.Instance.OnAllUnitsAttackRequested -= Attack;
    }

    public abstract void Attack();
}
