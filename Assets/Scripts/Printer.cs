using System.Collections.Generic;
using UnityEngine;

public class Printer : MonoBehaviour
{    
    public List<string> texts;
    public bool buttonHasBeenClicked = false;

    [Header("Debug only")]
    public int counter = -1;
    public string currentText = "";

    //Editor time only (no build, no runtime)
    void OnDrawGizmos()
    {
        // == uguaglianza
        // != diseguaglianza       
        // > maggiore
        // < minore
        // >= 
        // <=


        if (buttonHasBeenClicked)
        {
            counter++; //aggiunge a se stesso +1
            currentText = GetCurrentTextByCounter();
            buttonHasBeenClicked = false;
        }        
    }    

    string GetCurrentTextByCounter()
    {
        if (CounterIsInRange())
        {
            return texts[counter];
        }
        else
        {
            return texts[texts.Count - 1];
        }        
    }

    bool CounterIsInRange()
    {
        return counter <= texts.Count - 1;
    }
}
