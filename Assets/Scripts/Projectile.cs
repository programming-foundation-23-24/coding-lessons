﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed = 5;

    private void Update()
    {
        transform.position += transform.forward * Time.deltaTime * speed;
    }
}
