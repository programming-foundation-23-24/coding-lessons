﻿public class AttackButton : BaseButton
{
    protected override void OnClick()
    {
        GameManager.Instance.RequestAllUnitsAttack();
    }
}
