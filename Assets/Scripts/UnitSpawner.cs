﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitSpawner : MonoBehaviour
{
    public float radomRangePosition = 4f;
    public List<UnitData> allUnitsData = new List<UnitData>();
    Dictionary<string, BaseUnit> allUnitsByName = new Dictionary<string, BaseUnit>();

    int total;
    int randomIndex;

    void Start()
    {
        GameManager.Instance.OnUnitSpawnRequested += SpawnByProbability;
        GameManager.Instance.OnUnitSpawnByNameRequested += OnUnitSpawnByNameRequested;

        foreach (UnitData objectData in allUnitsData)
        {
            total += objectData.unitProbability;

            if (allUnitsByName.ContainsKey(objectData.unitName))
            {
                Debug.LogError($"Dictionary already contains {objectData.unitName}! Add aborted.");
                continue;
            }
            
            allUnitsByName.Add(objectData.unitName, objectData.unitPrefab);
        }
    }

    void OnUnitSpawnByNameRequested(string unitName)
    {
        if (!allUnitsByName.ContainsKey(unitName))
        {
            Debug.LogError($"Not found {unitName}!");
            return;
        }
        
        SpawnUnit(allUnitsByName[unitName]);
    }

    void OnDestroy()
    {
        GameManager.Instance.OnUnitSpawnRequested -= SpawnByProbability;
        GameManager.Instance.OnUnitSpawnByNameRequested -= OnUnitSpawnByNameRequested;
    }

    void SpawnByProbability()
    {
        randomIndex = Random.Range(0, total);
        int currentIndex = 0;

        foreach (UnitData objectData in allUnitsData)
        {
            currentIndex += objectData.unitProbability;

            if (randomIndex < currentIndex)
            {
                PickData(objectData);
                break;
            }
        }
    }

    void PickData(UnitData pickedData)
    {
        Debug.Log($"Rolled {randomIndex}. {pickedData.unitName} has been spawned!");
        SpawnUnit(pickedData.unitPrefab);
    }

    void SpawnUnit(BaseUnit unitPrefab)
    {
        Instantiate(unitPrefab, GetRandomPosition(), Quaternion.identity);
    }

    Vector3 GetRandomPosition()
    {
        float x = Random.Range(-radomRangePosition, radomRangePosition);
        float y = Random.Range(-radomRangePosition, radomRangePosition);
        float z = Random.Range(-radomRangePosition, radomRangePosition);

        return new Vector3(x, y, z);
    }
}
