﻿public class SpawnButton : BaseButton
{
    protected override void OnClick()
    {
        GameManager.Instance.RequestUnitSpawn();
    }
}
