using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitCreationUI : MonoBehaviour
{
    public Button creationButton;
    public TMPro.TMP_InputField creationInputField;

    void Start()
    {
        creationButton.onClick.AddListener(OnCreationButtonClick);
    }

    void OnDestroy()
    {
        creationButton.onClick.RemoveListener(OnCreationButtonClick);
    }

    void OnCreationButtonClick()
    {
        GameManager.Instance.RequestUnitySpawnByName(creationInputField.text);
    }

    void Update()
    {
        creationButton.interactable = creationInputField.text != "";
    }
}
